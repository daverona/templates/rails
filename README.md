# daverona/rails

[![pipeline status](https://gitlab.com/daverona/templates/rails/badges/master/pipeline.svg)](https://gitlab.com/daverona/templates/rails/-/commits/master)
[![coverage report](https://gitlab.com/daverona/templates/rails/badges/master/coverage.svg)](https://gitlab.com/daverona/templates/rails/-/commits/master)

This is a repository for Docker images of Rails application.

* GitLab source repository: [https://gitlab.com/daverona/templates/rails](https://gitlab.com/daverona/templates/rails)
* GitLab container registry: [https://gitlab.com/daverona/templates/rails/container\_registry/](https://gitlab.com/daverona/templates/rails/container_registry/)

## Prerequisites

* [Docker](https://docs.docker.com/get-docker/) is installed.
* [Traefik](https://docs.traefik.io/) is installed. ([This](http://gitlab.com/daverona/docker-compose/traefik) is recommended.)
* `aeon.example` needs to map to 127.0.0.1 for development. ([dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) is recommended.)

## Quick Start

### Development

To test-drive the development version:

```bash
mv master.key.should-not-be-kept-in-git-repository master.key
# NOTE. I only keep secret.key.should-not-be-kept-in-git-repository for demonstration.
# NOTE. You should NEVER keep master.key in any repository or in any Docker image.
cp docker-compose.draft.yml docker-compose.yml
cp .env.draft .env
docker-compose build
docker-compose up -d
```

Then visit [http://aeon.example/](http://aeon.example/).
If "Yay! You're on Rails!" page shows up, it's a success.

(Optional) To skip the image building, do the following instead of `docker-compose build`:

```bash
docker image pull registry.gitlab.com/daverona/templates/rails:draft
docker image tag registry.gitlab.com/daverona/templates/rails:draft daverona/rails:draft
```

### Production

To test-drive the production version:

```bash
mv master.key.should-not-be-kept-in-git-repository master.key
# NOTE. I only keep secret.key.should-not-be-kept-in-git-repository for demonstration.
# NOTE. You should NEVER keep master.key in any repository or in any Docker image.
cp docker-compose.final.yml docker-compose.yml
cp .env.final .env
docker-compose build --build-arg RAILS_MASTER_KEY="$(cat master.key)"
docker-compose up -d
```

Then visit [http://aeon.example/](http://aeon.example/).
If "The page you were looking for doesn't exist." page in red shows up, it's a success.

(Optional) To skip the image building, do the following instead of `docker-compose build ...`:

```bash
docker image pull registry.gitlab.com/daverona/templates/rails:final
docker image tag registry.gitlab.com/daverona/templates/rails:final daverona/rails:final
```

## Bootstrapping

To start a project with a clean slate:

```bash
# Stop container
docker-compose down

# Empty source
rm -rf source

# Create a project (specify Rails version if you will)
docker container run --rm \
  --volume $PWD/source:/source \
  ruby:2.5 \
  bash -c "\
    echo -e \"source 'https://rubygems.org'\ngem 'rails', '~>5'\" > Gemfile \
    && touch Gemfile.lock \
    && bundle install --jobs=\$(nproc) \
    && rails new /source --force --database=sqlite3 \
    && cd /source \
    && bundle add sidekiq --version '~>6' \
    && chown -R $(id -u):$(id -g) /source"

# Move master.key out of source directory
# NOTE. You should NEVER keep it in any repository or in any Docker image.
mv source/config/master.key .
```

`source` contains a brand-new project.  To run, do "Quick Start" section.

## Why Should I Use This?

1. *Fast*: Dockerfiles take as less time as possible to build images once `source` changes.
2. *Small*: Dockerfiles contain no build-time dependencies.
3. *Clean*: `source` directory can serve as project root *without* Docker.
4. *Complete*: Dockerfiles contain exact services required in common projects for each mode.

### Installed Services

#### Development

* Rails app server (`bundle exec rails server`)
* [Sidekiq](https://github.com/mperham/sidekiq/wiki) server
* Redis server
* Supervisor (process control)

#### Production

* Puma server
* Nginx server
* [Sidekiq](https://github.com/mperham/sidekiq/wiki) server
* Logrotate service
* Redis server
* Supervisor (process control)

## Cloning

Time to make this template yours:

```bash
git clone https://gitlab.com/daverona/templates/rails.git
cd rails
rm -rf .git
git init
git add .
git commit -m "Clone daverona/rails"
```

You have done for a local repository. If you have set up an empty remote repository, say `git@gitserver.com:rails.git`, set and push to remote:

```bash
git remote add origin git@gitserver.com:rails.git
git push --set-upstream origin master
```

### Directory Structure

```
docker/                   ; Docker directory                
  draft/                    ; deveopment version
    Dockerfile
    docker-entrypoint.sh
    supervisor/
      puma.ini                ; Puma server
      redis.ini               ; Redis server
      sidekiq.ini             ; Sidekiq server
      supervisor.ini          ; Supervisor
  final/                    ; production version
    Dockerfile
    docker-entrypoint.sh
    logrotate/                ; Logrotate configuration
      ...
    nginx/                    ; Nginx configuration
      ...
    supervisor/
      clond.ini               ; Logrotate service (by crond)
      nginx.ini               ; Nginx server
      puma.ini                ; Puma server
      redis.ini               ; Redis server
      sidekiq.ini             ; Sidekiq server
      supervisor.ini          ; Supervisor
source/                   ; Rails project root
  ...
```
## Notes

* *Never* store `master.key` in any repository or in any Docker image.
* If you lose `master.key`, all credentials encrypted in `credentials.yml.enc` will be lost too.
* `RAILS_ENV` is hard-coded in Docker images and does not need to be specified in `.env` file.

### Development

* If `source/Gemfile` and/or `source/Gemfile.lock` are updated, build your development Docker image again.
* To update `source/config/credentials.yml.enc`, run the development container and do:

```bash
# Skills on vi editor is required
docker container exec --interactive --tty aeon \
  ash -c "EDITOR='vi' rails credentials:edit"
```

* To change `master.key` (and hence `credentials.yml.enc`), run the development container and do:

```bash
# Show the credentials in plain text
docker container exec aeon ash -c "rails credentials:show"
# Copy the contents shown above somewhere

docker-compose down

# Skills on vi editor is required
# Remove old pair and make new one
docker container run --rm \
  --interactive --tty \
  --volume $PWD/source:/app \
  daverona/rails:draft \
  ash -c "\
    rm -rf /app/config/master.key \
    && rm -rf /app/config/credentials.yml.enc \
    && EDITOR='vi' rails credentials:edit"
# Paste the copied contents, save and quit vi above

# Move master.key out of source directory
# NOTE. You should NEVER keep it in any repository or in any Docker image.
mv source/config/master.key .

docker-compose up -d
```

### Production

* If `master.key` and/or `credentials.yml.enc` are changed, build your production Docker image again.
* The stock `master.key` should not be used. Generate a new one as shown above.
* Distribute the following only:
  * `master.key`
  * `.env.final`
  * `docker-compose.final.yml`
  * production Docker image

## Advertisement

This is a part of my *Web Framework in Docker* series. Please check out:

* daverona/django: [https://gitlab.com/daverona/templates/django](https://gitlab.com/daverona/templates/django) &mdash; Python
* daverona/laravel: [https://gitlab.com/daverona/templates/laravel](https://gitlab.com/daverona/templates/laravel) &mdash; PHP
* daverona/rails: [https://gitlab.com/daverona/templates/rails](https://gitlab.com/daverona/templates/rails) &mdash; Ruby
* daverona/vue: [https://gitlab.com/daverona/templates/vue](https://gitlab.com/daverona/templates/vue) &mdash; JavaScript

## References

* [https://guides.rubyonrails.org/v5.2/](https://guides.rubyonrails.org/v5.2/)

<!-- https://gist.github.com/satendra02/1b335b06bfc5921df486f26bd98e0e89 -->
<!-- https://levelup.gitconnected.com/three-methods-to-share-rails-assets-with-nginx-f39c90bb7d68 -->
<!-- bundle exec rails db:reset -->
<!-- https://medium.com/cedarcode/rails-5-2-credentials-9b3324851336 -->
